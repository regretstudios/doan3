import mongoose from 'mongoose';
import Promise from 'bluebird'
import DatabaseUrl from '../../config.js';


function ConnectDatabase() {


    mongoose.connect(DatabaseUrl);
    mongoose.Promise = Promise;

    const db = mongoose.connection;

    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', () => {
        // we're connected!
        console.log(`connent success db to ${DatabaseUrl}`)
    });

}

export { ConnectDatabase }